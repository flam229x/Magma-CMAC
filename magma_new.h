#ifndef MAGMA_NEW_H
#define MAGMA_NEW_H

#include <inttypes.h>
#include <stdlib.h>
#include <string.h>

#define BLOCK_SIZE 4
typedef uint8_t vec[BLOCK_SIZE];


void Magma_Add(const uint32_t *a, const uint32_t *b, uint32_t *c);

void Magma_KeyDeployment(const uint32_t *Key);

void Magma_Add_32(const uint32_t *a, const uint32_t *b, uint32_t *c);

void Magma_t_trans(const uint32_t *in, uint32_t *out);

void Magma_g_trans(const uint32_t *k, const uint32_t *a, uint32_t *res);

void Magma_G_trans(const uint32_t *k, const uint64_t *a, uint64_t *res);

void Magma_G_star_trans(const uint32_t *k, const uint64_t *a, uint64_t *res);

void Magma_Encript(const uint64_t *blk, uint64_t *out);

#endif