#ifndef CMAC_H
#define CMAC_H

#define f_bit 0x8000000000000000

#include <inttypes.h>
#include "magma_new.h"

void CMAC_GenerateSecKeys(const uint64_t *R, uint64_t *K1, uint64_t *K2);

void Proc2(uint64_t *P);

void CMAC_Iter(uint8_t count_block, uint64_t *P, uint64_t *MAC, uint64_t *Cq_1);

void CMAC_ReturnHash(uint8_t ind, uint64_t *P, uint64_t *MAC, uint64_t *Cq_1, uint64_t *Key, uint8_t s);

#endif